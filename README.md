# Lilypond Resources

A series of resources to start to use Lilypond and to develop graphically useful stuff:
- [Lilypond website](http://lilypond.org/manuals)
- [Lilypond Snippet Repository](https://lsr.di.unimi.it/LSR/)
- [Introduction to Lilypond by Pasquale Citera at SMERM](https://github.com/SMERM/INTRO_LILYPOND/tree/master)
- [Music: Practice & Theory Stack Exchange](https://music.stackexchange.com/)